package gouda

import (
	"bytes"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/flosch/pongo2"

	"codeberg.org/momar/logg"
	"codeberg.org/momar/ternary"
)

// Y u so spaghetti?
// -> As Gouda is not built to be able to execute just a part of the steps (or the steps in a different order), the code is much easier to follow when having everything in the order of execution.

// Render executes the gouda pipeline to produce a static site according to the Config given
func (config Config) Render() {
	////////////////////////////
	// Step 0: validate config
	if config.Input == nil {
		logg.Error("An input directory is required")
		throw()
	}
	if config.Output == "" {
		config.Output = "./output"
	}
	if config.Languages == nil || len(config.Languages) == 0 {
		config.Languages = []string{""}
	}
	if config.Plugins == nil {
		config.Plugins = []Plugin{}
	}
	if config.Metadata == nil {
		config.Metadata = map[string]interface{}{}
	}
	if config.MetadataByLanguage == nil {
		config.MetadataByLanguage = map[string]map[string]interface{}{}
		for _, lang := range config.Languages {
			config.MetadataByLanguage[lang] = map[string]interface{}{}
		}
	}

	////////////////////////////
	// Step 1: parse templates
	logg.Info(logg.Colors.FormatBold + "Step 1/9:" + logg.Colors.FormatReset + " parse templates (.tpl files in template directory)")
	pongo2.RegisterFilter("link", func(in *pongo2.Value, param *pongo2.Value) (*pongo2.Value, *pongo2.Error) {
		s := strings.TrimPrefix(in.String(), "/")
		if !config.LinkIndexFiles && strings.HasSuffix(s, "/index.html") {
			s = strings.TrimSuffix(s, "index.html")
		} else if config.LinkIndexFiles && !strings.HasSuffix(s, "/index.html") && s != "index.html" {
			s = strings.TrimSuffix(s, "/") + "/index.html"
		}
		return pongo2.AsValue(s), nil
	})
	pongo2.RegisterFilter("sourcefile", func(in *pongo2.Value, param *pongo2.Value) (*pongo2.Value, *pongo2.Error) {
		if param.String() == config.Languages[0] {
			if _, err := config.Input.Open(in.String()); err == nil {
				return in, nil
			}
		}
		return pongo2.AsValue(strings.TrimSuffix(in.String(), filepath.Ext(in.String())) + "." + param.String() + filepath.Ext(in.String())), nil
	})
	pongo2.RegisterFilter("basename", func(in *pongo2.Value, param *pongo2.Value) (*pongo2.Value, *pongo2.Error) {
		return pongo2.AsValue(filepath.Base(in.String())), nil
	})
	var templates = map[string]Template{}
	if config.Templates != nil && walkFileSystem(config.Templates, func(path string, file http.File) error {
		// FIXME: doesn't work with subdirectories
		if filepath.Ext(path) != ".tpl" && filepath.Ext(path) != ".pongo" {
			return nil
		}
		templateType := filepath.Ext(path)
		path = strings.TrimSuffix(strings.TrimPrefix(path, "/"), templateType)
		logg.Field("name", path).Debug("Found template")
		source, err := ioutil.ReadAll(file)
		if err != nil {
			return err
		}
		var tpl Template
		if templateType == ".tpl" {
			tpl, err = template.New(path).Parse(string(source))
			if err != nil {
				return err
			}
		} else {
			t, err := pongo2.FromString(string(source))
			if err != nil {
				return err
			}
			tpl = PongoTemplate{t}
		}
		templates[path] = tpl
		return nil
	}) != nil {
		throw()
	}
	logg.Debug("%d templates found", len(templates))

	/////////////////////////////////////////////////////////////////////////////
	// Step 2: build source file context tree (including splitting by language)
	logg.Info(logg.Colors.FormatBold + "Step 2/9:" + logg.Colors.FormatReset + " build source context tree")
	inputRoot = &DirContext{
		Directories: map[string]*DirContext{},
		Files:       map[string]map[string]*Context{},
		path:        "/",
		config:      &config,
	}
	if walkFileSystem(config.Input, func(path string, file http.File) error {
		// Ignore hidden files
		if strings.Contains(path, "/.") {
			return nil
		}

		// Load the file contents
		content, err := ioutil.ReadAll(file)
		if err != nil {
			return err
		}

		// Set the language, fall back to the default language (e.g. helloworld.en.md -> "en" or the first language if "en" is not a valid language)
		ext := filepath.Ext(path)
		lang := strings.TrimPrefix(filepath.Ext(strings.TrimSuffix(path, ext)), ".")
		langExists := false
		for _, l := range config.Languages {
			if l == lang {
				langExists = true
			}
		}
		if !langExists {
			lang = config.Languages[0]
		}
		pathWithoutLang := ternary.If(langExists, strings.TrimSuffix(strings.TrimSuffix(path, ext), "."+lang)+ext, path).(string)

		// Create a context and insert it at the correct location into the context tree
		context := &Context{
			Content:  content,
			Metadata: map[string]interface{}{},
			Output:   path,
			language: lang,
			path:     pathWithoutLang,
			config:   &config,
		}
		fd := inputRoot.Goto(filepath.Dir(pathWithoutLang))
		fl := fd.Files[filepath.Base(pathWithoutLang)]
		if fl == nil {
			fl = map[string]*Context{}
			fd.Files[filepath.Base(pathWithoutLang)] = fl
		}
		fl[lang] = context
		return nil
	}) != nil {
		throw()
	}
	fileCount := 0
	inputRoot.Walk(func(context *Context) error {
		logg.Field("path", context.Path()).Debug("Found input file")
		fileCount++
		return nil
	})
	logg.Debug("%d input files found", fileCount)

	////////////////////////////////
	// Step 3: prepare all plugins
	logg.Info(logg.Colors.FormatBold + "Step 3/9:" + logg.Colors.FormatReset + " prepare all plugins")
	for i, p := range config.Plugins {
		if pp, ok := p.(PreparingPlugin); ok {
			if inputRoot.Ignore {
				continue
			}
			logg.Field("plugin", i).Debug("Preparing plugin")
			err := pp.Prepare(inputRoot)
			if err != nil {
				logg.Field("plugin", i).Error("Plugin preparation failed: %s", err)
				throw()
			}
		}
	}
	// Contexts could have changed by now, so we're reading all the files again
	files := []*Context{}
	inputRoot.Walk(func(context *Context) error {
		files = append(files, context)
		return nil
	})

	//////////////////////////////////////////////////////////////////////////////
	// Step 4: run all plugins for each file (each file in a seperate goroutine)
	logg.Info(logg.Colors.FormatBold + "Step 4/9:" + logg.Colors.FormatReset + " run all plugins for each file")
	fileCount = len(files)
	fileProcessed := make(chan bool)
	process := func(context *Context, returnChannel chan bool) {
		for i, p := range config.Plugins {
			if context.Ignore {
				continue
			}
			logg.Field("path", context.Path()).Field("plugin", i).Debug("Processing file with plugin")
			err := p.Process(context)
			if err != nil {
				logg.Field("path", context.Path()).Field("plugin", i).Error("Processing failed for plugin: %s", err)
				throw()
			}
		}
		returnChannel <- true
	}
	for _, context := range files {
		go process(context, fileProcessed)
	}
	for fileCount > 0 {
		<-fileProcessed
		fileCount--
	}

	////////////////////////////////
	// Step 3: finish all plugins
	logg.Info(logg.Colors.FormatBold + "Step 5/9:" + logg.Colors.FormatReset + " finish all plugins")
	for i, p := range config.Plugins {
		if pp, ok := p.(FinishingPlugin); ok {
			if inputRoot.Ignore {
				continue
			}
			logg.Field("plugin", i).Debug("Finishing plugin")
			err := pp.Finish(inputRoot)
			if err != nil {
				logg.Field("plugin", i).Error("Plugin finishing failed: %s", err)
				throw()
			}
		}
	}
	// Contexts could have changed by now, so we're reading all the files again
	files = []*Context{}
	inputRoot.Walk(func(context *Context) error {
		files = append(files, context)
		return nil
	})

	////////////////////////////////////////
	// Step 5: apply template to all files
	logg.Info(logg.Colors.FormatBold + "Step 6/9:" + logg.Colors.FormatReset + " apply the correct template to every file")
	for _, context := range files {
		if context.Template == "" || context.Ignore || context.Output == "" {
			continue
		}
		tpl, ok := templates[context.Template]
		if !ok {
			logg.Field("path", context.Path()).Error("Template \"%s\" not found", context.Template)
			throw()
		}
		outpath := context.Output
		if context.Language() != config.Languages[0] {
			outpath = filepath.Join("/"+context.Language(), "./"+context.Output)
		}
		buffer := bytes.NewBuffer([]byte{})
		err := tpl.Execute(buffer, pongo2.Context{
			"Root": strings.TrimPrefix(strings.Repeat("../", strings.Count(filepath.Clean(outpath), "/")), "."),
			"Site": merge(config.Metadata, config.MetadataByLanguage[context.Language()]),
			"Meta": context.Metadata,
			"Data": template.HTML(context.Content),
			"In":   context.Path(),
			"Out":  context.Output,
			"Lang": context.Language(),
			"Prefix": func(lang string) string {
				root := strings.TrimPrefix(strings.Repeat("../", strings.Count(filepath.Clean(outpath), "/")), ".")
				langPrefix := ternary.If(lang == config.Languages[0] && !config.IncludeDefaultLanguage, "", lang+"/").(string)
				return root + langPrefix
			},
			"IsEqual": func(path1 string, path2 string) bool {
				path1 = strings.TrimSuffix(path.Join(strings.TrimSuffix(strings.TrimPrefix(path1, "/"), "/index.html")), "/")
				path2 = strings.TrimSuffix(path.Join(strings.TrimSuffix(strings.TrimPrefix(path2, "/"), "/index.html")), "/")
				if path1 == "index.html" {
					path1 = ""
				}
				if path2 == "index.html" {
					path2 = ""
				}
				return path1 == path2
			},
			"IsPrefix": func(prefix string, path1 string) bool {
				prefix = strings.TrimSuffix(path.Join(strings.TrimSuffix(strings.TrimPrefix(prefix, "/"), "/index.html")), "/") + "/"
				path1 = strings.TrimSuffix(path.Join(strings.TrimSuffix(strings.TrimPrefix(path1, "/"), "/index.html")), "/") + "/"
				if path1 == "index.html" {
					path1 = ""
				}
				if prefix == "index.html" {
					prefix = ""
				}
				return strings.HasPrefix(path1, prefix)
			},
			"Conf": &config,
		})
		if err != nil {
			logg.Field("path", context.Path()).Error("Template \"%s\" produced an error: %s", context.Template, err)
			throw()
		}
		context.Content = buffer.Bytes()
		logg.Field("path", context.Path()).Debug("Template \"%s\" produced %d bytes of output", context.Template, len(context.Content))
	}

	///////////////////////////////////
	// Step 6: clear output directory
	logg.Info(logg.Colors.FormatBold + "Step 7/9:" + logg.Colors.FormatReset + " clear output directory")
	oldFiles, err := ioutil.ReadDir(config.Output)
	if err != nil && os.IsNotExist(err) {
		err := os.MkdirAll(config.Output, 0755)
		if err != nil {
			logg.Field("path", config.Output).Error("Couldn't create output directory: %s", err)
			throw()
		}
	} else if err != nil {
		logg.Field("path", config.Output).Error("Couldn't read output directory: %s", err)
		throw()
	} else {
		for _, file := range oldFiles {
			p := filepath.Join(config.Output, file.Name())
			if err := os.RemoveAll(p); err != nil {
				logg.Field("path", p).Error("Couldn't delete file or directory from output directory: %s", err)
				throw()
			}
		}
	}

	//////////////////////////////
	// Step 7: copy theme assets
	logg.Info(logg.Colors.FormatBold + "Step 8/9:" + logg.Colors.FormatReset + " copy theme assets")
	fileCount = 0
	if config.Assets != nil && walkFileSystem(config.Assets, func(path string, file http.File) error {
		fileCount++
		logg.Field("path", path).Debug("Found asset file")
		in, err := config.Assets.Open(path)
		if err != nil {
			return err
		}
		defer in.Close()

		err = os.MkdirAll(filepath.Dir(filepath.Join(config.Output, "."+path)), 0755)
		if err != nil {
			return err
		}

		out, err := os.Create(filepath.Join(config.Output, "."+path))
		if err != nil {
			return err
		}
		defer out.Close()
		_, err = io.Copy(out, in)
		if err != nil {
			return err
		}
		return nil
	}) != nil {
		throw()
	}
	logg.Debug("%d asset files found", fileCount)

	///////////////////////////////
	// Step 8: write output files
	logg.Info(logg.Colors.FormatBold + "Step 9/9:" + logg.Colors.FormatReset + " write output files")
	fileCount = 0
	for _, context := range files {
		if context.Ignore || context.Output == "" {
			continue
		}
		// Choose directory according to the language
		outpath := filepath.Join(config.Output, context.Output)
		if config.IncludeDefaultLanguage || context.Language() != config.Languages[0] {
			outpath = filepath.Join(config.Output, context.Language(), "./"+context.Output)
		}

		if _, err := os.Stat(outpath); !os.IsNotExist(err) {
			logg.Field("path", context.Path()).Field("target", context.Output).Error("Output file already exists!")
			throw()
		}

		err := os.MkdirAll(filepath.Dir(outpath), 0755)
		if err != nil {
			logg.Field("path", context.Path()).Error("Couldn't create output directory: %s", err)
			throw()
		}

		fileCount++
		err = ioutil.WriteFile(outpath, context.Content, 0644)
		if err != nil {
			logg.Field("path", context.Path()).Field("target", context.Output).Error("Couldn't write file to output directory: %s", err)
			throw()
		}
		logg.Field("path", context.Path()).Field("target", context.Output).Debug("Wrote output file")
	}
	logg.Debug("%d output files written", fileCount)

	logg.Info(logg.Colors.Green + logg.Colors.FormatBold + "Build Successful!")
}

func throw() {
	logg.Error(logg.Colors.Red + logg.Colors.FormatBold + "Build Failed!")
	os.Exit(1)
}

func merge(maps ...map[string]interface{}) map[string]interface{} {
	var result = map[string]interface{}{}
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

type Template interface {
	Execute(io.Writer, interface{}) error
}
type PongoTemplate struct {
	Template *pongo2.Template
}

func (t PongoTemplate) Execute(w io.Writer, data interface{}) error {
	return t.Template.ExecuteWriter(data.(pongo2.Context), w)
}

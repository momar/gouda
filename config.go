package gouda

import "net/http"

type Config struct {
	Input                  http.FileSystem
	Templates              http.FileSystem
	Assets                 http.FileSystem
	Output                 string
	LinkIndexFiles         bool
	Languages              []string
	Plugins                []Plugin
	Metadata               map[string]interface{}
	MetadataByLanguage     map[string]map[string]interface{}
	IncludeDefaultLanguage bool
}

type Plugin interface {
	Process(*Context) error
}
type PreparingPlugin interface {
	Plugin
	Prepare(*DirContext) error
}
type FinishingPlugin interface {
	Plugin
	Finish(*DirContext) error
}

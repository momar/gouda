# gouda

## A short example

```go
package main

import (
	"net/http"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/gouda/plugins/metadata"
	"codeberg.org/momar/gouda/plugins/markdown"
	"codeberg.org/momar/gouda/plugins/blog"
	"codeberg.org/momar/gouda/plugins/calendar"
	"codeberg.org/momar/gouda/plugins/tableofcontents"
	"codeberg.org/momar/gouda/plugins/templatemap"
)

func main() {
	gouda.Config{
		Input:     http.Dir("src"),
		Templates: http.Dir("tpl"),
		Assets:    http.Dir("res"),
		Output:    "out",
		Languages: []string{"de", "en"},
		Plugins: []gouda.Plugin{
			&templatemap{
				"*.md": "page",
				"/news/**.md": "blog",
			},
			&metadata{
				"Navigation": "/navigation.yaml",
			},
			&markdown{},
			&blog{
				Articles: "/news/**.md", // articles to parse
				Output: "/page/",        // /page/1/ ... /page/132/ ...
				OutputFirst: "/",        // First page is /, /page/1/ will redirect there
				PerPage: 5,              // 5 articles per page
				CreationFromGit: true,   // Get creator and creation date from git history
			},
			&calendar{"https://example.org/mycalendar.ics"},
			&tableofcontents{},
		},
	}.Render()
}
```

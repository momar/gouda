package markdown

import (
	"bytes"
	"path/filepath"
	"strings"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/logg"
	"codeberg.org/momar/ternary"
	"github.com/microcosm-cc/bluemonday"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
	"gopkg.in/yaml.v2"
)

type Plugin struct {
	UnsafeSource bool
	Include      []string
	Exclude      []string
}

func (p *Plugin) Prepare(context *gouda.DirContext) error {
	p.Include = ternary.Default(p.Include, []string{"*.md"}).([]string)
	p.Exclude = ternary.Default(p.Exclude, []string{}).([]string)
	return nil
}

func (p *Plugin) Process(context *gouda.Context) error {
	if !has(p.Include, context.Glob) || has(p.Exclude, context.Glob) {
		return nil
	}
	context.Output = strings.TrimSuffix(strings.TrimSuffix(context.Path(), filepath.Ext(context.Path())), "/index") + "/index.html"
	// parse frontmatter
	if string(context.Content[:4]) == "---\n" {
		content := strings.SplitN(string(context.Content), "\n---\n", 2)
		frontmatter := map[string]interface{}{}
		err := yaml.Unmarshal([]byte(content[0]), frontmatter)
		if err == nil {
			mergeInto(context.Metadata, frontmatter)
			if len(content) > 1 {
				context.Content = []byte(content[1])
			} else {
				context.Content = []byte{}
			}
		} else {
			logg.Field("path", context.Path()).Warn("Error when parsing frontmatter in markdown plugin: %s", err)
		}
	}
	// parse markdown
	md := goldmark.New(
		goldmark.WithExtensions(extension.GFM),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
		goldmark.WithRendererOptions(
			html.WithHardWraps(),
			html.WithUnsafe(),
		),
	)
	var buf bytes.Buffer
	if err := md.Convert(context.Content, &buf); err != nil {
		panic(err)
	}
	context.Content = buf.Bytes()
	if p.UnsafeSource {
		context.Content = bluemonday.UGCPolicy().SanitizeBytes(context.Content)
	}
	return nil
}

func has(slice []string, test func(string) bool) bool {
	for _, expr := range slice {
		if test(expr) {
			return true
		}
	}
	return false
}

func mergeInto(into map[string]interface{}, from map[string]interface{}) {
	for key, value := range from {
		if _, ok := into[key]; ok {
			if t, ok := into[key].(map[string]interface{}); ok {
				if s, ok := value.(map[string]interface{}); ok {
					mergeInto(t, s)
					continue
				}
			}
		}
		into[key] = value
	}
}

package filemeta

import (
	"errors"

	"codeberg.org/momar/gouda"
	"gopkg.in/yaml.v2"
)

type Plugin map[string]string

func (p *Plugin) Prepare(context *gouda.DirContext) error {
	// read files from values & write to metadata by keys
	for key, filename := range *p {
		file := context.GetFile(filename)
		for _, c := range file {
			c.Ignore = true
		}
		for _, lang := range context.Config().Languages {
			var content interface{}
			var err error
			if c, ok := file[lang]; ok {
				err = yaml.Unmarshal(c.Content, &content)
			} else if c, ok := file[context.Config().Languages[0]]; ok {
				err = yaml.Unmarshal(c.Content, &content)
			} else {
				return errors.New(filename + " is not available in the default language!")
			}
			if err != nil {
				return err
			}
			context.Config().MetadataByLanguage[lang][key] = content
		}
	}
	return nil
}

func (p *Plugin) Process(context *gouda.Context) error {
	return nil
}

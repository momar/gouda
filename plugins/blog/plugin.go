package blog

import (
	"math"
	"os/exec"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/logg"
	"github.com/btubbs/datetime"
	"github.com/k3a/html2text"
)

type Plugin struct {
	Articles            string
	Output              string
	OutputFirst         string
	PerPage             uint
	SummaryLength       uint
	InferFromRepository string
	IndexTemplate       string
	ArticleTemplate     string
	articles            map[string][]*gouda.Context
	pages               map[string]Page
}

type Page struct {
	Name string
	Date string
}

func (p *Plugin) Prepare(context *gouda.DirContext) error {
	if p.Articles == "" {
		p.Articles = "**.md"
	}
	if p.Output == "" {
		p.Output = "/blog/"
	}
	if p.OutputFirst == "" {
		p.OutputFirst = p.Output
	}
	if p.PerPage == 0 {
		p.PerPage = 5
	}
	if p.SummaryLength == 0 {
		p.SummaryLength = 1000
	}

	if !strings.HasSuffix(p.Output, "/") {
		p.Output += "/"
	}
	if !strings.HasSuffix(p.OutputFirst, "/") {
		p.OutputFirst += "/"
	}

	p.articles = map[string][]*gouda.Context{}
	for _, lang := range context.Config().Languages {
		p.articles[lang] = []*gouda.Context{}
	}

	return nil
}

func (p *Plugin) Process(context *gouda.Context) error {
	if !context.Glob(p.Articles) {
		return nil
	}
	// git magic
	if p.InferFromRepository != "" {
		git := exec.Command("git", "log", "--remove-empty", "--pretty=format:%aI %an", "--", strings.TrimPrefix(context.Path(), "/"))
		git.Dir = p.InferFromRepository
		result, err := git.Output()
		logg.Field("path", context.Path()).Debug("%s", result)
		if err != nil {
			logg.Field("path", context.Path()).Warn("Error when running 'git log' in blog plugin: %s", err)
		}
		lines := strings.Split(string(result), "\n")
		fields := strings.SplitN(lines[len(lines)-1], " ", 2)

		if _, ok := context.Metadata["Author"]; !ok && len(fields) > 1 {
			context.Metadata["Author"] = fields[1]
		}
		if _, ok := context.Metadata["Date"]; !ok {
			context.Metadata["Date"] = fields[0]
		}
	}
	if _, ok := context.Metadata["Summary"]; !ok {
		sum := html2text.HTML2Text(string(context.Content))
		sum = regexp.MustCompile(`[ \t]+`).ReplaceAllString(sum, " ")
		sum = regexp.MustCompile(`\s{2,}`).ReplaceAllString(sum, "\n")
		sum = strings.TrimSpace(sum)
		if uint(len(sum)) > p.SummaryLength+10 {
			sum = strings.TrimSpace(sum[:p.SummaryLength]) + "…"
		}
		context.Metadata["Summary"] = sum
	}
	if p.ArticleTemplate != "" {
		context.Template = p.ArticleTemplate
	}
	p.articles[context.Language()] = append(p.articles[context.Language()], context)
	return nil
}

func (p *Plugin) Finish(context *gouda.DirContext) error {

	for _, article := range p.articles[context.Config().Languages[0]] {
		for i := 1; i < len(context.Config().Languages); i++ {
			lang := context.Config().Languages[i]
			if _, ok := context.GetFile(article.Path())[lang]; !ok {
				f := context.NewFile(article.Path(), lang)
				f.Output = article.Output
				f.Content = article.Content
				f.Metadata = article.Metadata
				f.Written = article.Written
				f.Ignore = article.Ignore
				f.Template = article.Template
				p.articles[lang] = append(p.articles[lang], f)
			}
		}
	}

	for _, lang := range context.Config().Languages {
		n := 0
		for _, x := range p.articles[lang] {
			if !x.Ignore {
				p.articles[lang][n] = x
				n++
			}
		}
		p.articles[lang] = p.articles[lang][:n]

		sort.Slice(p.articles[lang], func(i int, j int) bool {
			ta, _ := datetime.Parse(p.articles[lang][i].Metadata["Date"].(string), time.UTC)
			tb, _ := datetime.Parse(p.articles[lang][j].Metadata["Date"].(string), time.UTC)
			if ta.IsZero() {
				return true
			}
			if ta.Before(tb) || tb.IsZero() {
				return false
			}
			return true
		})

		pages := int(math.Ceil(float64(len(p.articles[lang])) / float64(p.PerPage)))
		for i := 1; i <= pages; i++ {
			f := context.NewFile(p.Output+strconv.Itoa(i)+"/index.html", lang)
			f.Template = p.IndexTemplate
			f.Metadata["Page"] = i
			f.Metadata["PageCount"] = pages
			f.Metadata["Articles"] = p.articles[lang][(i-1)*int(p.PerPage) : minInt(len(p.articles[lang]), i*int(p.PerPage))]
			if i > 2 {
				f.Metadata["PreviousPage"] = strings.TrimPrefix(p.Output+strconv.Itoa(i-1)+"/index.html", "/")
			} else if i > 1 {
				f.Metadata["PreviousPage"] = strings.TrimPrefix(p.Output+"index.html", "/")
			}
			if i < pages {
				f.Metadata["NextPage"] = strings.TrimPrefix(p.Output+strconv.Itoa(i+1)+"/index.html", "/")
			}
			f.Output = f.Path()
			f.Written = true
			if i == 1 && p.OutputFirst != "" {
				logg.Debug("Hoi %d %s", i, lang)
				f2 := context.NewFile(p.OutputFirst+"/index.html", lang)
				f2.Template = p.IndexTemplate
				f2.Metadata = f.Metadata
				f2.Output = f2.Path()
				f2.Written = true
			}
		}
	}
	return nil
}

func minInt(a, b int) int {
	if a > b {
		return b
	}
	return a
}

package autotitle

import (
	"bytes"
	"strings"
	"path/filepath"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/logg"
	"github.com/PuerkitoBio/goquery"
)

type Plugin struct {
}

func (p *Plugin) Prepare(context *gouda.DirContext) error {
	return nil
}

func (p *Plugin) Process(context *gouda.Context) error {
	if t, ok := context.Metadata["Title"]; filepath.Ext(context.Output) == ".html" && (!ok || t == "") {
		context.Metadata["Title"] = ""
		doc, err := goquery.NewDocumentFromReader(bytes.NewReader(context.Content))
		if err != nil {
			logg.Field("path", context.Path()).Warn("Error when parsing HTML in autotitle plugin: %s", err)
		} else {
			context.Metadata["Title"] = doc.Find("h1").First().Text()
		}
		if context.Metadata["Title"] == "" {
			context.Metadata["Title"] = strings.TrimSuffix(filepath.Base(context.Path()), filepath.Ext(context.Path()))
		}
	}
	return nil
}

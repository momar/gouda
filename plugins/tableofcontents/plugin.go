package tableofcontents

import (
	"bytes"
	"path/filepath"
	"strconv"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/logg"
	"github.com/PuerkitoBio/goquery"
)

type Plugin struct {
	Nested bool
}

type Heading struct {
	Text     string
	ID       string
	Level    int
	Children []*Heading
}

func (p *Plugin) Prepare(context *gouda.DirContext) error {
	return nil
}

func (p *Plugin) Process(context *gouda.Context) error {
	if _, ok := context.Metadata["TOC"]; filepath.Ext(context.Output) == ".html" && !ok {
		headings := []*Heading{}
		doc, err := goquery.NewDocumentFromReader(bytes.NewReader(context.Content))
		if err != nil {
			logg.Field("path", context.Path()).Warn("Error when parsing HTML in tableofcontents plugin: %s", err)
			return nil
		}
		doc.Find("h1, h2, h3, h4, h5, h6").Each(func(i int, s *goquery.Selection) {
			level, _ := strconv.Atoi(goquery.NodeName(s)[1:])
			if !p.Nested {
				headings = append(headings, &Heading{
					s.Text(),
					s.AttrOr("id", ""), //s.,
					level,
					nil,
				})
			} else {
				heading := &Heading{
					s.Text(),
					s.AttrOr("id", ""), //s.,
					level,
					[]*Heading{},
				}
				if len(headings) > 0 {
					h := headings[len(headings)-1]
					if h.Level >= level {
						headings = append(headings, heading)
					} else {
						for i := 1; i < level; i++ {
							if len(h.Children) > 0 && h.Children[len(h.Children)-1].Level < level {
								h = h.Children[len(h.Children)-1]
							}
						}
						h.Children = append(h.Children, heading)
					}
				} else {
					headings = append(headings, heading)
				}
			}
		})
		context.Metadata["TOC"] = headings
	}
	return nil
}

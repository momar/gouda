package templatemap

import "codeberg.org/momar/gouda"

type Plugin map[string]string

func (p *Plugin) Process(context *gouda.Context) error {
	for expr, templ := range *p {
		if context.Glob(expr) {
			context.Template = templ
		}
	}
	return nil
}

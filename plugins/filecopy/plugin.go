package filecopy

import (
	"mime"
	"path/filepath"

	"codeberg.org/momar/gouda"
)

type Plugin []string

func (p *Plugin) Prepare(context *gouda.DirContext) error {
	return context.Walk(func(file *gouda.Context) error {
		for _, targettype := range *p {
			match, err := filepath.Match(targettype, mime.TypeByExtension(filepath.Ext(file.Path())))
			if err != nil {
				return err
			}
			if match && file.Output == "" {
				file.Output = file.Path()
				break
			}
		}
		return nil
	})
}

func (p *Plugin) Process(context *gouda.Context) error {
	return nil
}

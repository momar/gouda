module codeberg.org/momar/gouda

go 1.12

require (
	codeberg.org/momar/logg v0.0.0-20190603184436-64108125abb6
	codeberg.org/momar/ternary v0.0.0-20190206091215-7d184a388958
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/bmatcuk/doublestar v1.1.1
	github.com/btubbs/datetime v0.1.0
	github.com/flosch/pongo2 v0.0.0-20190505152737-8914e1cf9164
	github.com/k3a/html2text v0.0.0-20190714173509-955615037597
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/yuin/goldmark v1.0.9
	gopkg.in/yaml.v2 v2.2.2
)

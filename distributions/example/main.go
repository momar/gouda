package main

import (
	"net/http"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/gouda/plugins/autotitle"
	"codeberg.org/momar/gouda/plugins/blog"
	"codeberg.org/momar/gouda/plugins/filemeta"
	"codeberg.org/momar/gouda/plugins/markdown"
	"codeberg.org/momar/gouda/plugins/tableofcontents"
	"codeberg.org/momar/gouda/plugins/templatemap"
	"codeberg.org/momar/logg"
)

func main() {
	gouda.Config{
		Input:          http.Dir("src"),
		Templates:      http.Dir("tpl"),
		Assets:         http.Dir("res"),
		Output:         "out",
		LinkIndexFiles: true,
		Languages:      []string{"en", "de"},
		Metadata: map[string]interface{}{
			"Title": "Gouda Example",
		},
		Plugins: []gouda.Plugin{
			&filemeta.Plugin{
				"Navigation": "/navigation.yaml",
			},
			&markdown.Plugin{},
			&blog.Plugin{
				Articles:            "/blog/**.md",
				Output:              "/blog/",
				OutputFirst:         "/blog/",
				PerPage:             2,
				InferFromRepository: "src",
				IndexTemplate:       "blog",
			},
			&templatemap.Plugin{
				"*.md":  "test",
				"*.txt": "index",
			},
			&autotitle.Plugin{},
			&tableofcontents.Plugin{},
			&TestPlugin{},
		},
	}.Render()
}

type TestPlugin struct {
}

func (*TestPlugin) Process(c *gouda.Context) error {
	logg.Tag("TestPlugin").Debug("Process %s", c.Path())
	if c.Path() == "/example.txt" {
		c.Output = "/index.html"
	}
	return nil
}

func (*TestPlugin) Prepare(c *gouda.DirContext) error {
	logg.Tag("TestPlugin").Debug("Prepare %s", c.Path())
	return nil
}

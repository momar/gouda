<!doctype html>
<html>
	<head>
		<title>Hello World</title>
		<meta charset="utf-8">
		<style>
			body { font-family: Arial, “Helvetica Neue”, Helvetica, sans-serif; margin: 0; padding: 25px; display: flex; box-sizing: border-box; min-height: 100vh; justify-content: center; align-items: center; text-align: center; flex-direction: column; }
			main { text-align: left; }
			a { color: #08f; }
			small { line-height: 1.5; }
			pre, code { background: #eee; }
			pre { padding: 5px; }
		</style>
	</head>
	<body>
		<img src="{{ .Root }}mouse.svg">
		<main style="margin: 1em 0; font-size: 1.2em">{{ .Data }}</main>
		<small>
			Pages {{range .Site.Navigation}} &bull; <a href="{{if .Page}}{{call $.Prefix $.Lang}}{{.Page}}{{else}}{{.Link}}{{end}}">{{.Title}}</a>{{end}}<br>
			Languages {{range .Conf.Languages}} &bull; <a href="{{call $.Prefix .}}{{$.Out}}">{{.}}</a>{{end}}<br>
			<br>
			built with <a href="https://codeberg.org/momar/gouda">gouda</a><br>
			<a href="https://commons.wikimedia.org/wiki/File:Creative-Tail-Animal-mouse.svg">Mouse &copy; Creative Tail</a>
		</small>
	</body>
</html>

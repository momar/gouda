# The Blog Plugin

## Usage
```go
&blog.Plugin{
	Articles: "/news/**.md", // articles to parse
	Output: "/page/",        // /page/1/ ... /page/132/ ...
	OutputFirst: "/",        // First page is /, /page/1/ will redirect there
	PerPage: 5,              // 5 articles per page
	InferFromRepository: "src", // Get creator and creation date from git history
}
```

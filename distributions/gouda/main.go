package main

import (
	"codeberg.org/momar/gouda/plugins/filecopy"
	"net/http"

	"codeberg.org/momar/gouda"
	"codeberg.org/momar/gouda/plugins/autotitle"
	"codeberg.org/momar/gouda/plugins/filemeta"
	"codeberg.org/momar/gouda/plugins/markdown"
	"codeberg.org/momar/gouda/plugins/tableofcontents"
	"codeberg.org/momar/gouda/plugins/templatemap"
)

//go:generate go-bindata -fs -prefix theme/ theme/...

func main() {
	// TODO: populate Site.Git
	// TODO: auto-detect languages
	// TODO: auto-generate title and navigation if not set
	// TODO: parse markdown in footer
	// TODO: parse .yml files
	// TODO: command line arguments for custom directories
	gouda.Config{
		Input:          http.Dir("docs"),
		Templates:      AssetFile(),
		Assets:         AssetFile(),
		Output:         "docs.html",
		Languages: []string{"en", "de"},
		LinkIndexFiles: true,
		Metadata: map[string]interface{}{},
		Plugins: []gouda.Plugin{
			&filemeta.Plugin{"Config": "/gouda.yml"},
			&markdown.Plugin{},
			&templatemap.Plugin{"*.md":  "main"},
			&autotitle.Plugin{},
			&tableofcontents.Plugin{true},
			&filecopy.Plugin{"image/*"},
		},
	}.Render()
	// TODO: build PDF according to the page navigation, auto-sort unlisted files to the end
}

# Introduction

**Gouda** is a documentation aggregator that can take a lot of documentation sources from your project, and output a beautiful & uniform HTML or PDF documentation with everything included - Markdown, JavaDocs, ESDocs, GoDoc, RAML & Swagger, and a lot more.

It is based on the [**Gouda Framework**](/gouda-framework), which is a framework for static site generators, allowing you to flexibly use custom plugins to generate static HTML sites of any kind.

## Basic Usage

1. Put all your Markdown documentation into the `docs` folder of your repository
2. To add your code documentaton, add a `.yml` file (e.g. `docs/javadocs.yml`), and specify where the documentation should be sourced from:
   ```yaml
   type: javadoc
   source: ../src
   ```
3. Generate the HTML files: `docker run --rm -v "$PWD:/src" codeberg.org/momar/gouda`
4. Check out your beautiful new documentation: `xdg-open docs.html/index.html`

## Usage on Codeberg

1. Follow only steps 1 & 2 of the "Basic Usage" instructions
2. Commit & Push to the "master" branch
3. Visit [docs.codeberg.org/owner.../repo...](https://docs.codeberg.org/momar/gouda)

## Advanced Usage
- [Available Types](/generating)

## References
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [Docker Setup](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

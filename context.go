package gouda

import (
	"path/filepath"
	"strings"

	"codeberg.org/momar/logg"
	"github.com/bmatcuk/doublestar"
)

type Context struct {
	Output   string
	Content  []byte
	Metadata map[string]interface{}
	Written  bool
	Ignore   bool
	Template string
	language string
	path     string
	config   *Config
}

func (c *Context) Config() *Config {
	return c.config
}
func (c *Context) Language() string {
	return c.language
}
func (c *Context) Path() string {
	return c.path
}
func (c *Context) Glob(expr string) bool {
	p := c.Path()
	if !strings.Contains(expr, "**") && !strings.Contains(expr, "/") {
		p = filepath.Base(p)
	}
	matches, _ := doublestar.Match(expr, p)
	return matches
}
func (c *Context) Move(to string) {
	oldParent := inputRoot.Goto(filepath.Dir(c.path))
	newParent := inputRoot.Goto(filepath.Dir(to))
	delete(oldParent.Files, filepath.Base(c.path))
	if newParent.Files[filepath.Base(to)] == nil {
		newParent.Files[filepath.Base(to)] = map[string]*Context{}
	}
	newParent.Files[filepath.Base(to)][c.language] = c
	c.path = to
}
func (c *Context) Parent() *DirContext {
	return inputRoot.Goto(filepath.Dir(c.path))
}

var inputRoot *DirContext

type DirContext struct {
	Ignore      bool
	Directories map[string]*DirContext
	Files       map[string]map[string]*Context
	path        string
	config      *Config
}

func (c *DirContext) Config() *Config {
	return c.config
}
func (c *DirContext) Path() string {
	return c.path
}
func (c *DirContext) Move(to string) {
	oldParent := inputRoot.Goto(filepath.Dir(c.path))
	newParent := inputRoot.Goto(filepath.Dir(to))
	delete(oldParent.Directories, filepath.Base(c.path))
	newParent.Directories[filepath.Base(to)] = c
	c.path = to
}
func (c *DirContext) Parent() *DirContext {
	return inputRoot.Goto(filepath.Dir(c.path))
}

func (c *DirContext) Goto(path string) *DirContext {
	pathSlices := strings.Split(filepath.Clean(path), "/")
	current := c
	for _, dir := range pathSlices {
		if dir == "" {
			continue
		}
		subdir, ok := current.Directories[dir]
		if !ok {
			current.Directories[dir] = &DirContext{
				Directories: map[string]*DirContext{},
				Files:       map[string]map[string]*Context{},
				path:        filepath.Clean(filepath.Join(current.path+"/", "./"+dir)),
				config:      c.config,
			}
			current = current.Directories[dir]
		} else {
			current = subdir
		}
	}
	return current
}

func (c *DirContext) Walk(fn func(*Context) error) error {
	var globalErr error
	for _, f := range c.Files {
		for _, fl := range f {
			err := fn(fl)
			if err != nil {
				logg.Field("path", fl.path).Field("lang", fl.language).Error("%s", err)
				if globalErr == nil {
					globalErr = err
				}
			}
		}
	}
	for _, d := range c.Directories {
		err := d.Walk(fn)
		if err != nil && globalErr == nil {
			globalErr = err
		}
	}
	return nil
}

func (c *DirContext) Glob(expr string) []*Context {
	result := []*Context{}
	c.Walk(func(context *Context) error {
		p := context.Path()
		if !strings.Contains(expr, "**") && !strings.Contains(expr, "/") {
			p = filepath.Base(p)
		}
		matches, err := doublestar.Match(expr, p)
		if matches {
			result = append(result, context)
		}
		return err
	})
	return result
}

func (c *DirContext) NewFile(name string, language string) *Context {
	f := &Context{
		Content:  []byte{},
		Metadata: map[string]interface{}{},
		path:     filepath.Clean(filepath.Join(c.path+"/", "./"+name)),
		language: language,
		config:   c.config,
	}

	languageExists := false
	for _, lang := range c.config.Languages {
		if lang == language {
			languageExists = true
		}
	}
	if !languageExists {
		logg.Field("path", f.path).Error("Couldn't create file with invalid language %s", language)
		throw()
	}

	fd := c.Goto(filepath.Dir(name))
	fl := fd.Files[filepath.Base(name)]
	if fl == nil {
		fl = map[string]*Context{}
		fd.Files[filepath.Base(name)] = fl
	}
	fl[language] = f
	return f
}

func (c *DirContext) GetFile(name string) map[string]*Context {
	d := c.Goto(filepath.Dir(name))
	result := map[string]*Context{}
	return d.Files[filepath.Base(name)]
	return result
}

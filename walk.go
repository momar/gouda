package gouda

import (
	"net/http"

	"codeberg.org/momar/logg"
)

func walkFileSystem(fs http.FileSystem, fn func(string, http.File) error) error {
	root, err := fs.Open("/")
	if err != nil {
		logg.Field("path", "/").Error("%s", err)
		return err
	}

	info, err := root.Stat()
	if err != nil {
		logg.Field("path", "/").Error("%s", err)
		return err
	}

	if info.IsDir() {
		return walkDir(fs, "/", root, fn)
	}
	err = fn("/", root)
	if err != nil {
		logg.Field("path", "/").Error("%s", err)
	}
	return err
}

func walkDir(fs http.FileSystem, path string, dir http.File, fn func(string, http.File) error) error {
	files, err := dir.Readdir(-1)
	if err != nil {
		logg.Field("path", path).Error("%s", err)
		return err
	}

	var globalErr error
	for _, file := range files {
		p := path + file.Name()
		f, err := fs.Open(p)
		if err != nil {
			logg.Field("path", p).Error("%s", err)
		} else {
			if file.IsDir() {
				err = walkDir(fs, p+"/", f, fn)
			} else {
				err = fn(p, f)
				if err != nil {
					logg.Field("path", p).Error("%s", err)
				}
			}
		}
		if err != nil && globalErr == nil {
			globalErr = err
		}
	}
	return globalErr
}
